-----------------------------------------------------------------------------
--
-- Module      :  Main
-- Copyright   :  (c) Bartłomiej Wróblewski 2020
-- License     :  BSD3
--
-- Maintainer  :  b.wroblewsky@gmail.com
-- Stability   :  experimental
-- Portability :  portable
--
-- | Testing basic properties of Data.CoalitionalGames via QuickCheck.
--
-----------------------------------------------------------------------------

module Main (
    main
) where

import System.Exit
import Control.Monad (unless)

import qualified Data.Map.Strict as Map
import Data.Map.Strict((!))
import qualified Data.Set as Set

import Test.QuickCheck

import qualified Data.CoalitionalGames as CG


-----------------------------------------------------------------------------
-- Auxiliary generators
-----------------------------------------------------------------------------

limit :: Int
limit = 3

genNonNegative :: (Arbitrary a, Num a) => Gen a
genNonNegative = fmap abs arbitrary

genPositive :: (Arbitrary a, Num a, Ord a) => Gen a
genPositive = suchThat genNonNegative (0 <)

genWeightedEdge :: Int -> Gen (CG.WeightedEdge Int)
genWeightedEdge k = do
    x <- choose (0, k - 1)
    y <- choose (0, k - 1)
    w <- genPositive :: Gen Int
    return (x, y, fromIntegral w)

-----------------------------------------------------------------------------
-- Voting Game Generators
-----------------------------------------------------------------------------

-- | Standard Voting Game
genVotingGame :: Gen (CG.CoalitionalGame Int)
genVotingGame = do
    k <- choose (1, limit)
    quota <- genPositive
    weight <- vectorOf k genNonNegative

    return $ CG.votingGame (Set.fromList [0..k - 1]) quota (weight !!)

-- | Voting Game with a dictator, who needs to be in a coalition in order to win
genDictatorVotingGame :: Gen (CG.CoalitionalGame Int)
genDictatorVotingGame = do
    k <- choose (1, limit)
    weight <- vectorOf (k - 1) genNonNegative
    let dictator = sum weight + 1

    return $ CG.votingGame (Set.fromList [0..k-1]) dictator ((dictator:weight) !!)


dictatorPayoffVector :: CG.PayoffVector Int
dictatorPayoffVector = Map.fromSet (\i -> if i == 0 then 1 else 0) (Set.fromList [0..limit])


-----------------------------------------------------------------------------
-- Induced Subgraph Game Generators
-----------------------------------------------------------------------------

-- | Induced Subgraph Game with integer edges

genInducedSubgraphGame :: Gen (CG.CoalitionalGame Int)
genInducedSubgraphGame = do
    k <- choose (1, limit)
    edges <- listOf $ genWeightedEdge k

    return $ CG.inducedSubgraphGame (Set.fromList [0..k - 1]) edges


-----------------------------------------------------------------------------
-- Test
-----------------------------------------------------------------------------

main = do
  let tests = [ quickCheckResult $ forAll genVotingGame CG.isMonotone
              , quickCheckResult $ forAll genVotingGame $ \game -> CG.payoff game Set.empty == 0

              , quickCheckResult $ forAll genDictatorVotingGame CG.isSuperadditive
              , quickCheckResult $ forAll genDictatorVotingGame $ \game -> CG.payoff game (Set.singleton 0) == 1
              , quickCheckResult $ forAll genDictatorVotingGame $ \game -> CG.isCore game dictatorPayoffVector
              , quickCheckResult $ forAll genDictatorVotingGame $ \game -> (CG.shapleyValue game ! 0) > 0.99
              , quickCheckResult $ forAll genDictatorVotingGame $ \game -> (CG.banzhafValue game ! 0) > 0.99

              , quickCheckResult $ forAll genInducedSubgraphGame CG.isMonotone
              , quickCheckResult $ forAll genInducedSubgraphGame CG.isSuperadditive
              , quickCheckResult $ forAll genInducedSubgraphGame $ \game -> CG.payoff game Set.empty == 0
              ]

  success <- fmap (all isSuccess) . sequence $ tests
  unless success exitFailure

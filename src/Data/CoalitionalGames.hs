-----------------------------------------------------------------------------
--
-- Module      :  Data.CoalitionalGames
-- Copyright   :  (c) Bartłomiej Wróblewski 2020
-- License     :  BSD3
--
-- Maintainer  :  b.wroblewsky@gmail.com
-- Stability   :  experimental
-- Portability :  portable
--
-- | An implementation of topics of "Algorithmic Coalitional Games Theory" course at MIMUW.
--   Implementation created as a final project for "Advanced Functional Programming" course.
--
-----------------------------------------------------------------------------

module Data.CoalitionalGames (
    -- * Auxiliary Types
    Edge,
    WeightedEdge,
    SynergyGroup (..),
    MCRule (..),

    -- * Types
    Coalition,
    Partition,
    Payoff,
    PayoffVector,
    CoalitionalGame (..),

    -- * Construction
    votingGame,
    inducedSubgraphGame,
    synergyGroupsGame,
    mcNetGame,

    -- * Properties
    isMonotone,
    isSuperadditive,

    -- * Payoff Vectors
    coalitionPayoff,
    isCore,

    -- * Semi-values
    semiValue,
    shapleyValue,
    banzhafValue
) where

import qualified Data.List as List
import qualified Data.Map.Strict as Map
import Data.Map.Strict((!))
import qualified Data.Set as Set


-----------------------------------------------------------------------------
-- Auxiliary Types
-----------------------------------------------------------------------------

-- | Graph edge representation.
type Edge a = (a, a)
type WeightedEdge a = (a, a, Float)

-- | Synergy group representation.
data SynergyGroup a = SynergyGroup {
  group :: Coalition a,
  groupWeight :: Float
}

-- | Marginal contribution rule.
data MCRule a = MCRule {
  positive :: Coalition a,
  negative :: Coalition a,
  ruleWeight :: Float
}


-----------------------------------------------------------------------------
--  Types
-----------------------------------------------------------------------------

-- | Coalition representation.
type Coalition a = Set.Set a

-- | Partition representation.
type Partition a = Set.Set (Coalition a)

-- | Payoff function representation.
type Payoff a = Coalition a -> Float

-- | Coalitional Game various representations.
data CoalitionalGame a = CoalitionalGame {
  players :: Coalition a,
  payoff :: Payoff a
}

instance (Ord a, Show a) => Show (CoalitionalGame a) where
  show game = show (players game, memorize game)

-- | Payoff vector representation.
type PayoffVector a = Map.Map a Float

-----------------------------------------------------------------------------
--  Utilities
-----------------------------------------------------------------------------

-- | Memorize function. Used to avoid repeated calculation of coalition payoffs.
memorize :: Ord a => CoalitionalGame a -> Map.Map (Coalition a) Float
memorize game = Map.fromSet (payoff game) (Set.powerSet (players game))


-----------------------------------------------------------------------------
-- Construction
-----------------------------------------------------------------------------

-- | Coalitional game representing voting systems with weighted contributions of players.
-- | Value of a coalition is 1 if the sum of weights of players is at least equal to quota, 0 otherwise.
votingGame :: Coalition a -> Float -> (a -> Float) -> CoalitionalGame a
votingGame players quota weight = CoalitionalGame players payoff where
    payoff s = if sum (Set.map weight s) >= quota then 1 else 0


-- | Coalitional game representing cooperation graph with possible self loops.
-- | Value of a coalition is equal to the sum of edge weights in graph induced by players.
inducedSubgraphGame :: Ord a => Coalition a -> [WeightedEdge a] -> CoalitionalGame a
inducedSubgraphGame players edges = CoalitionalGame players payoff where
    payoff s = sum $ List.map (contribution s) edges
    contribution s (x, y, w) = if Set.member x s && Set.member y s then w else 0


-- | Coalitional game with predefined values for given coalitions.
-- | Value of a coalition is equal to the maximal sum of some partition of this coalition.
synergyGroupsGame :: Eq a => Coalition a -> [SynergyGroup a] -> CoalitionalGame a
synergyGroupsGame players groups = CoalitionalGame players payoff where
    payoff s = maximum $ Set.map payoff' (Set.powerSet s)
    payoff' s = sum $ List.map (contribution s) groups
    contribution s g = if group g == s then groupWeight g else 0


-- | Coalitional game with set of binary rules with associated weights, fully expressive.
-- | Value of a coalition is equal to the sum of weights of matched rules.
mcNetGame :: Ord a => Coalition a -> [MCRule a] -> CoalitionalGame a
mcNetGame players rules = CoalitionalGame players payoff where
    payoff s = sum $ List.map (contribution s) rules
    contribution s r = if Set.isSubsetOf (positive r) s && Set.disjoint (negative r) s then ruleWeight r else 0


-----------------------------------------------------------------------------
--  Properties
-----------------------------------------------------------------------------

-- | CG is monotone, if each coalition has at least as big payoff as it's sub-coalitions.
isMonotone :: Ord a => CoalitionalGame a -> Bool
isMonotone game = all monotone (Set.powerSet (players game)) where
    monotone s = payoff game s >= maximum (Set.map (without s) (players game))
    without s i = payoff game (Set.delete i s)

-- | CG is superadditive, if each two coalitions have at least as big payoff after merging.
isSuperadditive :: Ord a => CoalitionalGame a -> Bool
isSuperadditive game = all superadditive (Set.powerSet (players game)) where
    superadditive s = payoff game s >= maximum (Set.map (split s) (Set.powerSet s))
    split s t = payoff game t + payoff game (Set.difference s t)


-----------------------------------------------------------------------------
--  Payoff Vectors
-----------------------------------------------------------------------------

-- | Coalition payoff according to given payoff vector.
coalitionPayoff :: Ord a => PayoffVector a -> Coalition a -> Float
coalitionPayoff vector players = sum $ Set.map (vector !) players

-- | Payoff vector is in the core, if it divides whole payoff and for each coalitions it gives at least it's payoff.
isCore :: Ord a => CoalitionalGame a -> PayoffVector a -> Bool
isCore game vector = divides && satisfies where
    divides = coalitionPayoff vector (players game) == payoff game (players game)
    satisfies = all satisfied (Set.powerSet (players game))
    satisfied s = coalitionPayoff vector s >= payoff game s


-----------------------------------------------------------------------------
--  Semivalues
-----------------------------------------------------------------------------

-- | Semivalues are payoff vectors calculating methods satisfying linearity, positivity and symmetry rules.
-- | They are characterized by factor functions determining impact of marginal contribution based on size of coalition.
semiValue :: Ord a => (Int -> Float) -> CoalitionalGame a -> PayoffVector a
semiValue factor game = Map.fromSet playerValue (players game) where
    playerValue i = sum $ List.map (marginalValue i) $ Set.toList $ Set.powerSet $ Set.delete i (players game)
    marginalValue i s = factor (Set.size s) * (payoff game (Set.insert i s) - payoff game s)

-- | Shapley-Value is a payoff vector regarded as "most fair".
shapleyValue :: Ord a => CoalitionalGame a -> PayoffVector a
shapleyValue game = semiValue factor game where
    factor ss =
      (factorials !! ss) *
      (factorials !! (Set.size (players game) - ss - 1)) /
      (factorials !! Set.size (players game))

    factorials :: [Float]
    factorials = 1 : zipWith (*) factorials [1 ..]

-- | Banzhaf-Value is a widely used semi-value.
banzhafValue :: Ord a => CoalitionalGame a -> PayoffVector a
banzhafValue game = semiValue factor game where
    factor _ = 1 / (2 ^ (Set.size (players game) - 1))

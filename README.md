## Coalitional Games
An implementation of topics of "Algorithmic Coalitional Games Theory" course at MIMUW.

Implementation created as a final project for "Advanced Functional Programming" course.

#### Definition
![](documentation/definition.png)

#### Voting Games
![](documentation/voting.png)

#### Subgraph Induced Games
![](documentation/graph.png)

#### Payoff vectors
![](documentation/payoff.png)


##### Author
Bartłomiej Wróblewski

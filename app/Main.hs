module Main (
    main
) where

import Data.Set
import Data.CoalitionalGames

-----------------------------------------------------------------------------
-- Political parties
-----------------------------------------------------------------------------

data Party = LawAndJustice
           | UnitedPoland
           | Agreement
           | CivicPlatform
           | Modern
           | PolishInitiative
           | TheGreens
           | DemocraticLeftAlliance
           | Spring
           | Together
           | PolishPeopleParty
           | Kukiz15
           | UnionOfEuropeanDemocrats
           | KORWIN
           | NationalMovement
           | ConfederationOfThePolishCrown
           | GermanMinority deriving (Eq, Ord, Show)


unitedRight :: Coalition Party
unitedRight = fromList [ LawAndJustice, UnitedPoland, Agreement ]

civicCoalition :: Coalition Party
civicCoalition = fromList [ CivicPlatform, Modern, PolishInitiative, TheGreens ]

theLeft :: Coalition Party
theLeft = fromList [ DemocraticLeftAlliance, Spring, Together ]

polishCoalition :: Coalition Party
polishCoalition = fromList [ PolishPeopleParty, Kukiz15, UnionOfEuropeanDemocrats ]

confederation :: Coalition Party
confederation = fromList [ KORWIN, NationalMovement, ConfederationOfThePolishCrown ]

nonAffiliated :: Coalition Party
nonAffiliated = fromList [ GermanMinority ]

allParties :: Coalition Party
allParties = unions [unitedRight, civicCoalition, theLeft, polishCoalition, confederation, nonAffiliated]

-----------------------------------------------------------------------------
-- Seats
-----------------------------------------------------------------------------

seats :: Party -> Float
seats LawAndJustice = 198
seats UnitedPoland = 19
seats Agreement = 18
seats CivicPlatform = 119
seats Modern = 8
seats PolishInitiative = 4
seats TheGreens = 3
seats DemocraticLeftAlliance = 24
seats Spring = 19
seats Together = 6
seats PolishPeopleParty = 23
seats Kukiz15 = 6
seats UnionOfEuropeanDemocrats = 1
seats KORWIN = 5
seats NationalMovement = 5
seats ConfederationOfThePolishCrown = 1
seats GermanMinority = 1

-----------------------------------------------------------------------------
-- Quotas
-----------------------------------------------------------------------------

legislativeMajority :: Float
legislativeMajority = 231

constitutionalMajority :: Float
constitutionalMajority = 307

-----------------------------------------------------------------------------
-- Political games
-----------------------------------------------------------------------------

parliamentLegislativeMajority :: CoalitionalGame Party
parliamentLegislativeMajority = votingGame allParties legislativeMajority seats

parliamentConstitutionalMajority :: CoalitionalGame Party
parliamentConstitutionalMajority = votingGame allParties constitutionalMajority seats

-----------------------------------------------------------------------------
-- Main program
-----------------------------------------------------------------------------

main :: IO ()
main = do
  print $ "United Right (legilative): " ++ show (payoff parliamentLegislativeMajority unitedRight)
  print $ "United Right (constitutional): " ++ show (payoff parliamentConstitutionalMajority unitedRight)
  print $ "UR & CC (constitutional): " ++ show (payoff parliamentConstitutionalMajority $ union unitedRight civicCoalition)
  print $ "Constitutional Shapley-value: " ++ show (shapleyValue parliamentConstitutionalMajority)
